module.exports = {
  siteMetadata: {
    title: `interface`,
    siteUrl: `https://lulzboat.hugoworld.me/`,
  },
  plugins: [
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /assets/
        }
      }
    }
  ]
}