import axios from 'axios'
import { SERVER_URL } from '../utils/constants';

const getSounds = async () => {
    try {
        const resp = await fetch(`${SERVER_URL}/sounds`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
        const content = await resp.json()
        return content;
    } catch (error) {
        return null;
    }
}

const sendSound = async ({ searchValue, socketId, user }: { searchValue: string, socketId: string, user: DiscordUser }) => {
    try {
        const resp = await fetch(`${SERVER_URL}/sound`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                sound: searchValue,
                guildId: process.env.GATSBY_DISCORD_GUILD_ID,
                socketId,
                user,
            })
        })
        const content = await resp.json()
        return content;
    } catch (error) {
        return null;
    }
};

const getUser = async () => {
    const token = localStorage.getItem('discord-token');
    if (token === null) throw new Error('no_token');
    const resp = await axios.get('https://discord.com/api/users/@me', {
        headers: {
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    })
    return resp?.data
};

const api = {
    getSounds,
    sendSound,
    getUser,
};

export default api;
