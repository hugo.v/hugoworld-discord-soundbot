import React, { useEffect } from "react";
import { navigate } from "gatsby";

// styles
const pageStyles = {
  color: "white",
  padding: "96px",
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
}
const headingStyles = {
  marginTop: 0,
  marginBottom: 64,
  maxWidth: 320,
}

const LoginRedirect = () => {
  useEffect(() => {
    if (localStorage.getItem('discord-token') !== null) navigate('/');
    const fragment = new URLSearchParams(window.location.hash.slice(1));
    const [accessToken] = [fragment.get('access_token'), fragment.get('token_type')];

    if (accessToken) {
      localStorage.setItem('discord-token', accessToken);
      navigate('/');
    }
  });

  return (
    <main style={pageStyles}>
      <title>Login Redirect</title>
      <h1 style={headingStyles}>Login Redirect</h1>
    </main>
  )
}

export default LoginRedirect
