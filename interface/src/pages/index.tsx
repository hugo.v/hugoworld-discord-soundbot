import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useMediaQuery } from 'react-responsive';
import { navigate } from "gatsby";
import { io, Socket } from 'socket.io-client';
import { useFormik } from "formik";
import { useQuery, useMutation } from 'react-query';
import { AlertType } from 'react-alert';
import { Button, Heading } from '@pancakeswap-libs/uikit';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRandom } from '@fortawesome/free-solid-svg-icons';

// Components
import Trail from "../components/molecules/Trail/Trail";
import Player from '../components/molecules/Player/Player';
import SoundCard from '../components/atoms/SoundCard/SoundCard';
import Kerry from '../components/atoms/Kerry/Kerry';

// Hooks
import useAlertDisplay from '../hooks/useAlertDisplay';

// Services
import api from '../api/api';

// Assets
import Cookie from '../assets/cookie.svg';

// Constants
import { SERVER_URL } from '../utils/constants';

const SubHeader = styled.div`
  margin-top: 1rem;
`;

const Search = styled.div`
  margin: 2rem 0 2rem 0;
`;

const Random = styled.div`
  margin: 2rem 0 2rem 0;
`;

const Input = styled.input`
  color: #eee;
  padding: 0.6rem 1rem;
  font-size: 1.3rem;
  background-color: #262626;
  border: 1px solid #3a3a3a;
  border-radius: 13px;
  height: 48px;

  &:focus {
    color: #eee;
    padding: 0.6rem 1rem;
    background-color: #222;
    border: 1px solid rgb(177 152 231);
    border-radius: 13px;
    box-shadow: none;
  }
`;

const InputLabel = styled.label`
  color: rgb(162, 139, 212);
`;

const InputIcon = styled.span`
  color: rgb(162, 139, 212);
  fill: grey;
  min-width: 32px;
  padding-left: 12px;
  position: relative;
  float: right;
  margin-right: 15px;
  margin-top: -40px;
  color: #f3a327;
  display: ${props => props.hidden ? "none" : "initial"};
`;

const KerryWrapper = styled.div`
  height: 150px;
  width: 150px;
  border-radius: 10px;
`;

const IndexPage = () => {
  const [soundQueue, setSoundQueue] = useState([]);
  const [soundList, setSoundList] = useState([]);
  const [socket, setSocket] = useState<Socket | null>(null);
  const [soundPlaying, setSoundPlaying] = useState(null);
  const [favList, setFavList] = useState<string[]>([]);
  const { data } = useQuery('fetchSounds', api.getSounds);
  const { data: userData, isLoading: isUserLoading, isError: isUserError, error: userError } = useQuery('fetchUser', api.getUser);
  const sendSound = useMutation(api.sendSound);
  const { show: showAlert } = useAlertDisplay();
  const showCookie = useMediaQuery({ query: '(max-width: 768px)' });

  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: (submitValues) => {
      if (submitValues.search) handlePostSend(submitValues.search);
    },
  });

  const { values, handleSubmit, getFieldProps } = formik;

  useEffect(() => {
    if (localStorage.getItem('discord-token') === null) {
      navigate('/login/')
    }
  }, []);

  useEffect(() => {
    if (!isUserLoading && isUserError) {
      localStorage.removeItem('discord-token');
      navigate('/login')
    }
  }, [isUserError, isUserLoading]);

  useEffect(() => {
    if (socket) {
      socket.on("connect", () => {
      });
      socket.on('soundPlayed', (sound, user) => {
        showAlert({
          message: `${sound} à été ajouté à la queue par ${user.username}`,
          variant: "info" as AlertType,
        });
      });
      socket.on('userConnected', (user) => {
        const userInfo = JSON.parse(user)
        showAlert({
          message: {
            title: `${userInfo.username} s'est connecté`,
            content: (
              <div style={{ margin: '1rem .4rem .4rem 0' }}>
                <img
                  style={{ borderRadius: '50%' }}
                  alt=""
                  src={`https://cdn.discordapp.com/avatars/${userInfo.id}/${userInfo.avatar}.webp?size=48`}
                />
              </div>
            ),
          },
          variant: "info" as AlertType,
        });
      });
      socket.on('soundStartedPlaying', (sound) => {
        setSoundPlaying(sound);
      });
      socket.on('soundEndedPlaying', (sound) => {
        setSoundPlaying(null);
      });
      socket.on('updateQueue', (queue) => {
        setSoundQueue(queue.map((queueItem: { name: string, user: DiscordUser }) => ({
          sound: queueItem.name,
          avatar: queueItem.user.avatar,
          userId: queueItem.user.id,
          username: queueItem.user.username,
        })));
      });
    }
  }, [showAlert, socket]);

  useEffect(() => {
    if (userData && !isUserLoading) {
      setSocket(io(SERVER_URL, {
        query: {
          user: JSON.stringify(userData),
        }
      }));
    }
  }, [userData, isUserLoading]);

  useEffect(() => {
    if (values.search) setSoundList(data.filter((sound: string) => sound.includes(values.search)));
    else setSoundList(data);
  }, [values.search, data]);

  useEffect(() => {
    setSoundList(data || []);
    if (data) {
      const favorites = localStorage.getItem('favorites');
      if (favorites) {
        setFavList(JSON.parse(favorites))
      }
    }
  }, [data]);

  useEffect(() => {
    if (!sendSound.isLoading && sendSound.data) {
      const { error, sound } = sendSound.data;
      if (error) {
        showAlert({
          key: error,
          variant: "error" as AlertType
        })
      }
      else showAlert({
        message: `${sound} ajouté à la queue`,
        variant: "success" as AlertType
      });
    }
  }, [sendSound.isSuccess, sendSound.isLoading])

  const handlePostSend = async (searchValue: string) => {
    try {
      if (socket) await sendSound.mutate({ searchValue, socketId: socket.id, user: userData });
    } catch (error) {
      //
    }
  };

  const handleFavClick = (sound: string) => {
    let updated: string[];
    if (favList.find(s => s === sound)) updated = favList.filter(s => s !== sound);
    else updated = [...favList, sound];

    setFavList(updated);
    localStorage.setItem('favorites', JSON.stringify(updated))
  }

  return (
    <>
      <title>{`${process.env.GATSBY_SITE_NAME}`}</title>
      {!showCookie && (
        <div style={{
          position: 'absolute',
          top: '20px',
          left: '20px',
          width: '50px',
          height: '50px',
        }}>
          <Cookie />
        </div>)}
      <div style={{
        textAlign: 'center',
        marginTop: '2rem',
      }}>
        <Heading size="xl">{`Soundboard - ${process.env.GATSBY_SITE_NAME}`}</Heading>
      </div>
      {userData && !isUserLoading ?
        <div className="container">
          <div className="row align-items-end">
            <div className="col-md-6 col-sm-12">
              <Search>
                <form onSubmit={handleSubmit}>
                  <InputLabel htmlFor="search" className="form-label">
                    Recherche
                  </InputLabel>
                  <Input
                    autoComplete="off"
                    className="form-control"
                    placeholder="..."
                    {...getFieldProps("search")}
                  />
                  <InputIcon
                    hidden={false}
                  >
                    <svg
                      viewBox="0 0 24 24"
                      aria-hidden="true"
                    >
                      <g>
                        <path d="M21.53 20.47l-3.66-3.66C19.195 15.24 20 13.214 20 11c0-4.97-4.03-9-9-9s-9 4.03-9 9 4.03 9 9 9c2.215 0 4.24-.804 5.808-2.13l3.66 3.66c.147.146.34.22.53.22s.385-.073.53-.22c.295-.293.295-.767.002-1.06zM3.5 11c0-4.135 3.365-7.5 7.5-7.5s7.5 3.365 7.5 7.5-3.365 7.5-7.5 7.5-7.5-3.365-7.5-7.5z"></path>
                      </g>
                    </svg>
                  </InputIcon>
                </form>
              </Search>
            </div>

            <div className="col-md-3 col-sm-12">
              {soundList.length > 0 ?
                <Random>
                  <Button
                    onClick={() => handlePostSend(soundList[Math.floor(Math.random() * soundList.length)])}
                    variant="subtle"
                    scale="md"
                    external
                    endIcon={<FontAwesomeIcon icon={faRandom} className="ms-2" />}
                    mr="8px"
                  >
                    Random
                  </Button>
                </Random> : null}
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 col-sm-12">
              <Player
                soundPlaying={soundPlaying}
                queue={soundQueue}
              />
            </div>
            <div className="col-md-6 col-sm-12 d-flex align-items-center justify-content-center">
              <KerryWrapper>
                <Kerry />
              </KerryWrapper>
            </div>
          </div>
          {favList.length > 0 ?
            <>
              <div className="row">
                <SubHeader>
                  <Heading size="md">Favoris</Heading>
                </SubHeader>
                <Trail>
                  {favList.map((sound: string) => (
                    <div onClick={() => handlePostSend(sound)} key={`sound_${sound}`}>
                      <SoundCard onFavClick={handleFavClick} listType="favorites" isFavorite={true} label={sound} />
                    </div>
                  ))}
                </Trail>
              </div>
              <hr className="mt-3" />
            </> : null}
          <div className="row">
            <SubHeader>
              <Heading size="md">Liste</Heading>
            </SubHeader>
            <Trail>
              {soundList.map((sound: string) => (
                <div onClick={() => handlePostSend(sound)} key={`sound_${sound}`}>
                  <SoundCard onFavClick={handleFavClick} listType="default" isFavorite={favList.find(s => s === sound) !== undefined} label={sound} />
                </div>
              ))}
            </Trail>
          </div>
        </div> : <p>Chargement...</p>}
    </>);
}

export default IndexPage;
