import * as React from "react";
import styled from "styled-components";
import { Button } from '@pancakeswap-libs/uikit';

// styles
const pageStyles = {
  color: "white",
  padding: "96px",
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
}
const headingStyles = {
  marginTop: 0,
  marginBottom: 64,
  maxWidth: 320,
}

const Link = styled.a`
  text-decoration: none;
`;

const Login = () => {
  return (
    <main style={pageStyles}>
      <title>Login</title>
      <h1 style={headingStyles}>Login</h1>
      <Link href={process.env.GATSBY_OAUTH_REDIRECT}>
        <Button
          variant="primary"
          scale="md"
          external
          mr="8px"
        >
          Connexion par discord
        </Button>
      </Link>
    </main>
  )
}

export default Login
