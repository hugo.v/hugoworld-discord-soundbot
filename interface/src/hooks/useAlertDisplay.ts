import React, { useCallback } from 'react';
import { useAlert, AlertType } from 'react-alert'

type messagesTypes = {
    [key: string]: string,
}
const messages: messagesTypes = {
    "sound_not_found": "Son introuvable.",
    "not_in_voice_channel": "Le bot n'est pas connecté à un chan audio.",
    "unknown_error": "Erreur inconnue.",
}

type messageContent = {
    title: string,
    content: React.ReactNode,
}

const useAlertDisplay = () => {
    const alert = useAlert();

    const show = useCallback(({ message, key, variant }: { message?: messageContent | string | null, key?: string | null, variant: AlertType | undefined }) => {
        alert.show(key ? messages[key] : message, {
            type: variant,
        });
    }, [alert]);

    return { show };
}

export default useAlertDisplay;