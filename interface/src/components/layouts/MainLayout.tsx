import React from 'react';
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { transitions, positions, Provider as AlertProvider } from 'react-alert';
import { dark, Alert } from '@pancakeswap-libs/uikit';
// import AlertTemplate from 'react-alert-template-basic';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    background-color: #222;
    color: #eee;
  }
`

const AlertTemplate = ({ style, options, message, close }) => {
  const getVariant = () => {
    switch (options.type) {
      case ('info'):
        return 'info';
      case ('success'):
        return 'success';
      case ('error'):
        return 'danger';
    }
  }
  return (
    <div style={{ ...style, width: '300px' }}>
      {typeof message === 'string' ?
      <Alert variant={getVariant()} title={message} />
        : <Alert variant={getVariant()} title={message.title}>
          {message.content}
        </Alert>}
    </div>
  )
};

const options = {
  // you can also just use 'bottom center'
  position: positions.TOP_RIGHT,
  timeout: 5000,
  offset: '12px',
  transition: transitions.FADE
}

interface Props {
    children?: JSX.Element | JSX.Element[];
}

const MainLayout = ({ children }: Props) => {
    return (
      <ThemeProvider theme={dark}>
        <AlertProvider template={AlertTemplate} {...options}>
            <GlobalStyle />
            {children}
        </AlertProvider>
      </ThemeProvider>
    );
}

export default MainLayout;