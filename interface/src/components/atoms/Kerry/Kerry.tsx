import React, { Suspense } from 'react';
import { Canvas, useLoader } from 'react-three-fiber';
import { OrbitControls } from "@react-three/drei";
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader';

const KerryLily = () => {
    const kerryLMtl = useLoader(MTLLoader, '/3d/Kerry3LilyPad.mtl');
    const kerryL = useLoader(OBJLoader, '/3d/Kerry3LilyPad.obj', (loader) => {
        kerryLMtl.preload();
        loader.setMaterials(kerryLMtl);
    });
    
    kerryL.position.set(-5.3, -1, 0);

    return (
        <primitive object={kerryL} />
    );
};

const KerryModel = () => {
    const kerryMtl = useLoader(MTLLoader, '/3d/Kerry4Kerry.mtl');
    const kerry = useLoader(OBJLoader, '/3d/Kerry4Kerry.obj', (loader) => {
        kerryMtl.preload();
        loader.setMaterials(kerryMtl);
    });

    kerry.position.set(-5.3, -1, 0);

    return (
        <primitive object={kerry} />
    );
};

const Scene = () => {
    return (
        <>
            <ambientLight intensity={0.4} />
            <KerryModel />
            <KerryLily />
        </>
    );
};

const Kerry = (): JSX.Element | null => {
    return (
        <Canvas
            gl={{ antialias: true }}
            camera={{
                zoom: 1.7,
                position: [3, 1.3, 4],
            }}
        >
            <OrbitControls
                autoRotate
                autoRotateSpeed={6}
                enableZoom={false}
                enablePan={false}
            />
            <Suspense fallback={null}>
                <Scene />
            </Suspense>
        </Canvas>
    );
}

export default Kerry;