import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faTimes } from '@fortawesome/free-solid-svg-icons';
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons';

const Card = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin: 1rem 0;
    padding: 1rem 1.2rem 1rem 1.2rem;
    font-size: 1.2rem;
    border-radius: 13px;
    min-height: 4rem;
    background-color: #262626;
    border: 1px solid #3a3a3a;
    cursor: pointer;
    transition: all 200ms;

    &:hover {
        box-shadow: 0px 4px 5px 0px hsla(0, 0%, 0%, 0.14), 0px 1px 10px 0px hsla(0, 0%, 0%, 0.12),
            0px 2px 4px -1px hsla(0, 0%, 0%, 0.2);
        border: 1px solid rgb(177 152 231);
        transform: translateY(-1px);
    }

    &:hover > div {
        display: flex;
    }
`;

interface FavoriteProps {
    listType: string;
    isFavorite: boolean;
}

const Favorite = styled.div<FavoriteProps>`
    display: ${p => p.listType === 'default' && p.isFavorite ? "flex" : "none"};
    justify-content: center;
    align-items: center;
    height: 25px;
    width: 25px;
    opacity: 1;
    border-radius: 50%;
    position: absolute;
    right: -8px;
    top: -8px;
    background: #A28BD4;
    transition: all .2s;
`;

interface Props {
    label: string;
    isFavorite: boolean;
    listType: string;
    onFavClick: Function;
}

const SoundCard = ({ label, isFavorite = false, listType = 'default', onFavClick }: Props) => {
    const handleFavClick = (e: React.MouseEvent) => {
        e.stopPropagation();
        onFavClick(label);
    }

    const getFavIcon = () => {
        if (listType === 'favorites') return faTimes;
        if (!isFavorite) return farStar;
        return faStar;
    }

    return (
        <Card>
            <Favorite listType={listType} isFavorite={isFavorite} onClick={handleFavClick} className='active'>
                <FontAwesomeIcon size="xs" icon={getFavIcon()} />
            </Favorite>
            {label}
        </Card>
    );
}

export default SoundCard;