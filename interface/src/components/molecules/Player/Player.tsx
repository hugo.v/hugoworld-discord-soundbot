import React, { useState, useEffect } from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faBan } from '@fortawesome/free-solid-svg-icons';

const PlayerContainer = styled.div`
    position: relative;
    ${(props: { queueOpen: boolean }) => props.queueOpen ? 'display: flex;' : ''}
    ${(props: { queueOpen: boolean }) => props.queueOpen ? 'flex-direction: column;' : ''}
    margin: .5rem 0 1rem 0;
    padding: 1rem 1.4rem 1rem 1.4rem;
    font-size: 1.2rem;
    border-radius: 13px;
    background-color: #262626;
    border: 1px solid #3a3a3a;
    transition: all 200ms ease-in-out;
    height: ${(props: { queueOpen: boolean }) => props.queueOpen ? '20rem' : '5.7rem'};
`;

const QueueContainer = styled.div`
  flex: 1 1 auto;
  overflow-y: auto;
  margin-top: 1.5rem;
  &::-webkit-scrollbar {
    width: 6px;
  }
  
  &::-webkit-scrollbar-track {
    background: white;
    border-radius: 150px;
  }
   
  &::-webkit-scrollbar-thumb {
    background: #A28BD4;
    border-radius: 150px;
  }
  
  &::-webkit-scrollbar-thumb:hover {
    background: #9072d1;
  }
`;

const Title = styled.div`
    font-size: .9rem;
`;

const Playing = styled.div`
  display: flex;
  margin-top: 0.65rem;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const SoundPlaying = styled.span`
    font-size: 1.4rem;
`;

const SoundAndWaves = styled.div`
  display: flex;
  flex-direction: row;
  visibility: ${(props: { soundPlaying: string | null }) => props.soundPlaying ? 'visible' : 'hidden'}
`;

const TableBody = styled.tbody`
  & > tr {
    padding: .6rem;
    margin-right: .5rem;
    border-top: 1px solid #3a3a3a;
  }
  & > tr:first-child {
    border-top: none;
  }
`;

interface QueueItem {
  username: string;
  avatar: string;
  userId: string;
  sound: string;
}

interface Props {
  soundPlaying: string | null;
    queue: QueueItem[] | [];
}

const GlobalStyle = createGlobalStyle`
  #wave {
    height: 40px;
    width: 40px;
    fill: #A28BD4;
  }
  
  #Line_1 {
    -webkit-animation: pulse 1s infinite;
            animation: pulse 1s infinite;
    -webkit-animation-delay: 0.15s;
            animation-delay: 0.15s;
  }
  
  #Line_2 {
    -webkit-animation: pulse 1s infinite;
            animation: pulse 1s infinite;
    -webkit-animation-delay: 0.3s;
            animation-delay: 0.3s;
  }
  
  #Line_3 {
    -webkit-animation: pulse 1s infinite;
            animation: pulse 1s infinite;
    -webkit-animation-delay: 0.45s;
            animation-delay: 0.45s;
  }
  
  #Line_4 {
    -webkit-animation: pulse 1s infinite;
            animation: pulse 1s infinite;
    -webkit-animation-delay: 0.6s;
            animation-delay: 0.6s;
  }
  
  #Line_5 {
    -webkit-animation: pulse 1s infinite;
            animation: pulse 1s infinite;
    -webkit-animation-delay: 0.75s;
            animation-delay: 0.75s;
  }
  
  #Line_6 {
    -webkit-animation: pulse 1s infinite;
            animation: pulse 1s infinite;
    -webkit-animation-delay: 0.9s;
            animation-delay: 0.9s;
  }
  
  #Line_7 {
    -webkit-animation: pulse 1s infinite;
            animation: pulse 1s infinite;
    -webkit-animation-delay: 1.05s;
            animation-delay: 1.05s;
  }
  
  #Line_8 {
    -webkit-animation: pulse 1s infinite;
            animation: pulse 1s infinite;
    -webkit-animation-delay: 1.2s;
            animation-delay: 1.2s;
  }
  
  #Line_9 {
    -webkit-animation: pulse 1s infinite;
            animation: pulse 1s infinite;
    -webkit-animation-delay: 1.35s;
            animation-delay: 1.35s;
  }
  
  @-webkit-keyframes pulse {
    0% {
      transform: scaleY(1);
      transform-origin: 50% 50%;
    }
    50% {
      transform: scaleY(0.7);
      transform-origin: 50% 50%;
    }
    100% {
      transform: scaleY(1);
      transform-origin: 50% 50%;
    }
  }
  
  @keyframes pulse {
    0% {
      transform: scaleY(1);
      transform-origin: 50% 50%;
    }
    50% {
      transform: scaleY(0.7);
      transform-origin: 50% 50%;
    }
    100% {
      transform: scaleY(1);
      transform-origin: 50% 50%;
    }
  }
`

const Wave = () => (
    <div style={{ margin: '0 1rem' }}><svg id="wave" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 38.05">
        <path id="Line_1" data-name="Line 1" d="M0.91,15L0.78,15A1,1,0,0,0,0,16v6a1,1,0,1,0,2,0s0,0,0,0V16a1,1,0,0,0-1-1H0.91Z" />
        <path id="Line_2" data-name="Line 2" d="M6.91,9L6.78,9A1,1,0,0,0,6,10V28a1,1,0,1,0,2,0s0,0,0,0V10A1,1,0,0,0,7,9H6.91Z" />
        <path id="Line_3" data-name="Line 3" d="M12.91,0L12.78,0A1,1,0,0,0,12,1V37a1,1,0,1,0,2,0s0,0,0,0V1a1,1,0,0,0-1-1H12.91Z" />
        <path id="Line_4" data-name="Line 4" d="M18.91,10l-0.12,0A1,1,0,0,0,18,11V27a1,1,0,1,0,2,0s0,0,0,0V11a1,1,0,0,0-1-1H18.91Z" />
        <path id="Line_5" data-name="Line 5" d="M24.91,15l-0.12,0A1,1,0,0,0,24,16v6a1,1,0,0,0,2,0s0,0,0,0V16a1,1,0,0,0-1-1H24.91Z" />
        <path id="Line_6" data-name="Line 6" d="M30.91,10l-0.12,0A1,1,0,0,0,30,11V27a1,1,0,1,0,2,0s0,0,0,0V11a1,1,0,0,0-1-1H30.91Z" />
        <path id="Line_7" data-name="Line 7" d="M36.91,0L36.78,0A1,1,0,0,0,36,1V37a1,1,0,1,0,2,0s0,0,0,0V1a1,1,0,0,0-1-1H36.91Z" />
        <path id="Line_8" data-name="Line 8" d="M42.91,9L42.78,9A1,1,0,0,0,42,10V28a1,1,0,1,0,2,0s0,0,0,0V10a1,1,0,0,0-1-1H42.91Z" />
        <path id="Line_9" data-name="Line 9" d="M48.91,15l-0.12,0A1,1,0,0,0,48,16v6a1,1,0,1,0,2,0s0,0,0,0V16a1,1,0,0,0-1-1H48.91Z" />
    </svg></div>
);

const Player = ({ soundPlaying, queue }: Props) => {
  const [queueOpen, setQueueOpen] = useState(false);

  useEffect(() => {
    if (queue.length === 0) {
      setQueueOpen(false);
    }
  }, [queue])

  return (
    <>
      <Title>Son en cours</Title>
      <GlobalStyle />
      <PlayerContainer queueOpen={queueOpen}>
        <Playing>
          <FontAwesomeIcon style={{ cursor: 'pointer', margin: '0 !important', visibility: soundPlaying || queue.length ? 'hidden' : "hidden" }} icon={faBan} />
          <SoundAndWaves soundPlaying={soundPlaying}>
            <Wave />
            <SoundPlaying>{soundPlaying}</SoundPlaying>
            <Wave />
          </SoundAndWaves>
          <FontAwesomeIcon onClick={() => setQueueOpen(!queueOpen)} style={{ visibility: queue.length ? 'visible' : "hidden", cursor: 'pointer', transform: queueOpen ? 'rotate(180deg)' : '', transition: 'all 200ms' }} icon={faChevronDown} />
        </Playing>

        {queueOpen ?
          <QueueContainer>
            <table style={{ width: '100%' }}>
              <TableBody>
                {queue.map((queueItem, index) => (
                  <tr style={{ display: 'flex' }}>
                    <td style={{ display: 'flex', alignItems: 'center', marginRight: '.9rem', opacity: '.4', fontSize: '.9rem' }}>
                      {index + 1}
                    </td>
                    <td style={{ display: 'flex', alignItems: 'center', flex: '.7' }}>
                      {queueItem.sound}
                    </td>
                    <td style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end', flex: '.3', fontSize: '.95rem' }}>
                      <span style={{ opacity: '.4' }}>{queueItem.username}</span>
                      <img
                        style={{ marginLeft: '.8rem', borderRadius: '50%' }}
                        alt="avatar"
                        src={`https://cdn.discordapp.com/avatars/${queueItem.userId}/${queueItem.avatar}.webp?size=20`}
                      />
                    </td>
                  </tr>
                ))}
              </TableBody>
            </table>
          </QueueContainer>
          : null}
      </PlayerContainer>
    </>
  );
}

export default Player;