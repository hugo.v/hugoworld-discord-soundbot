export const SERVER_URL = process.env.NODE_ENV === "development"
? `http://${process.env.GATSBY_SERVER_URL}`
: `https://${process.env.GATSBY_SERVER_URL}`;