import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import http from 'http';
import { Server } from "socket.io";

import Config from '~/config/Config';
import SoundQueue from '~/queue/SoundQueue';

import CommandCollection from '../bot/CommandCollection';
import MessageHandler from '../bot/MessageHandler';
import SoundBot from '../bot/SoundBot';
import {
  AvatarCommand,
  ConfigCommand,
  IgnoreCommand,
  LanguageCommand,
  UnignoreCommand
} from '../commands/config';
import {
  HelpCommand,
  LastAddedCommand,
  MostPlayedCommand,
  PingCommand,
  WelcomeCommand
} from '../commands/help';
import {
  AddCommand,
  DownloadCommand,
  EntranceCommand,
  ExitCommand,
  ModifyCommand,
  RemoveCommand,
  RenameCommand,
  SearchCommand,
  SoundsCommand,
  StopCommand,
  TagCommand,
  TagsCommand
} from '../commands/manage';
import AttachmentDownloader from '../commands/manage/add/downloader/AttachmentDownloader';
import YoutubeDownloader from '../commands/manage/add/downloader/YoutubeDownloader';
import AttachmentValidator from '../commands/manage/add/validator/AttachmentValidator';
import YoutubeValidator from '../commands/manage/add/validator/YoutubeValidator';
import {
  ComboCommand,
  LoopCommand,
  NextCommand,
  RandomCommand,
  SkipCommand,
  SoundCommand
} from '../commands/sound';

export const config = new Config();

// Web server setup
const app = express();
const server = http.createServer(app);
const port = 3000;
const io = new Server(server, {
    cors: {
        methods: ['GET', "POST"],
        origin: '*'
    }
});

// const getAllSocketClients = () => Array.from( io.sockets.sockets.keys() );

io.on('connection', socket => {
  const { user } = socket.handshake.query;

  socket.broadcast.emit('userConnected', user);
    // socket.on('disconnect', () => {
    //  });
});

const jsonParser = bodyParser.json()

app.use(cors())

app.get('/sounds', (_req, res) => {
    const sounds = soundBot.getSoundsForWeb();
    res.status(200).json(sounds)
})

app.post('/sound', jsonParser, (req, res) => {
  const { sound, guildId, socketId, user } = req.body;

  const status = soundBot.playFromWeb(sound, guildId, user);
  switch (status) {
    case ("success"): {
      io.sockets.sockets.get(socketId)?.broadcast.emit('soundPlayed', sound, user);
      res.status(200).json({ ...req.body, success: true })
      break;
    }
    case ("sound_not_found"): {
      res.status(500).json({ error: 'sound_not_found', success: false })
      break;
    }
    case ("not_in_voice_channel"): {
      res.status(500).json({ error: 'not_in_voice_channel', success: false })
      break;
    }
    default: {
      res.status(500).json({ error: 'unknown_error', success: false })
      break;
    }
  }
})

server.listen(port, () => {
    console.info(`API listening at port ${port}`)
})

const queue = new SoundQueue(config, io);

const attachmentValidator = new AttachmentValidator(config);
const attachmentDownloader = new AttachmentDownloader(attachmentValidator);

const youtubeValidator = new YoutubeValidator();
const youtubeDownloader = new YoutubeDownloader(youtubeValidator);

const commands = [
  new PingCommand(),

  // SOUND PLAYING RELATED COMMANDS
  new SoundCommand(queue),
  new ComboCommand(queue),
  new RandomCommand(queue),
  new LoopCommand(queue),
  new NextCommand(queue),
  new SkipCommand(queue),
  new StopCommand(queue),

  // ENTRANCE / EXIT SOUNDS
  new EntranceCommand(),
  new ExitCommand(),

  // SOUND ADMINISTRATION COMMANDS
  new AddCommand(attachmentDownloader, youtubeDownloader),
  new SoundsCommand(config),
  new SearchCommand(),
  new ModifyCommand(),
  new RenameCommand(),
  new RemoveCommand(),
  new TagCommand(),
  new TagsCommand(),
  new DownloadCommand(),

  // HELP / INFO COMMANDS
  new WelcomeCommand(config),
  new HelpCommand(config),
  new LastAddedCommand(),
  new MostPlayedCommand(),

  // CONFIGURATION RELATED COMMANDS
  new AvatarCommand(config),
  new ConfigCommand(config),
  new LanguageCommand(config),
  new IgnoreCommand(),
  new UnignoreCommand()
];

const commandCollection = new CommandCollection(commands);
const messageHandler = new MessageHandler(commandCollection);

const soundBot = new SoundBot(config, commandCollection, messageHandler, queue);
interface SoundBotContainer {
  config: Config;
  soundBot: SoundBot;
}

export default {
  config,
  soundBot
} as SoundBotContainer;
